<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contoh Layout 4 Kolom</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
    crossorigin="anonymous"></script>

</head>

<body>

    <header class="container bg-primary text-white">
        <div class="row">
            <div class="col-12 py-4 text-center">
                <h1 class="display-2">Galleri Bootstrap</h1>
                <p class="lead">Panduan Belajar Bootstrap untuk Pemula</p>
            </div>
        </div>
    </header>

    <main class="container border p-md-5 p-2">
        <div class="row g-2 g-md-4">
            <div class="col-6 col-md-3">
                <img class="w-100" src="https://files.realpython.com/media/Getting-Started-with-Bootstrap-3_Watermarked.d9fd44fd999a.jpg" alt="bs">
            </div>
            <div class="col-6 col-md-3">
                <img class="w-100" src="https://files.realpython.com/media/Getting-Started-with-Bootstrap-3_Watermarked.d9fd44fd999a.jpg" alt="bs">
            </div>
            <div class="col-6 col-md-3">
                <img class="w-100" src="https://files.realpython.com/media/Getting-Started-with-Bootstrap-3_Watermarked.d9fd44fd999a.jpg" alt="bs">
            </div>
            <div class="col-6 col-md-3">
                <img class="w-100" src="https://files.realpython.com/media/Getting-Started-with-Bootstrap-3_Watermarked.d9fd44fd999a.jpg" alt="bs">
            </div>
            <div class="col-6 col-md-3">
                <img class="w-100" src="https://files.realpython.com/media/Getting-Started-with-Bootstrap-3_Watermarked.d9fd44fd999a.jpg" alt="bs">
            </div>
            <div class="col-6 col-md-3">
                <img class="w-100" src="https://files.realpython.com/media/Getting-Started-with-Bootstrap-3_Watermarked.d9fd44fd999a.jpg" alt="bs">
            </div>
            <div class="col-6 col-md-3">
                <img class="w-100" src="https://files.realpython.com/media/Getting-Started-with-Bootstrap-3_Watermarked.d9fd44fd999a.jpg" alt="bs">
            </div>
            <div class="col-6 col-md-3">
                <img class="w-100" src="https://files.realpython.com/media/Getting-Started-with-Bootstrap-3_Watermarked.d9fd44fd999a.jpg" alt="bs">
            </div>
        </div>
    </main>

    <footer class="container bg-light">
        <div class="row">
            <div class="col-12 py-4">
                &copy; 2021 Tutorial Bootstrap
            </div>
        </div>
    </footer>

</body>

</html>
