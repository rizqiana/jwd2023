<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Latihan 13 Javascript</title>
</head>
<body>
	<script type="text/javascript">
		var products = ["Senter", "Radio", "Antena", "Obeng"];
		document.write("<ol>");
		for (let i = 0; i < products.length; i++) {
			document.write(`<li>${products[i]}</li>`);
		}
		document.write("</ol>");

		var buah = ["Apel", "Jeruk", "Manggis"];

		buah.push("Semangka");
		buah[0] = "Belimbing";

		document.write("<ul>");
		for (let i = 0; i < buah.length; i++) {
			document.write(`<li>${buah[i]}</li>`);
		}
		document.write("</ul>");

		function sayHello() {
			console.log("Hello World");
		}

		sayHello();

		function kali(a, b) {
			var hasilKali = a * b;
			console.log(`Hasil kali ${a} * ${b} = ${hasilKali}`);
		}

		kali(3, 2);

		function bagi(a, b) {
			var hasilBagi = a / b;
			return hasilBagi;
		}

		var nilai1 = 20;
		var nilai2 = 5;
		var hasilPembagian = bagi(nilai1, nilai2) + 30;

		document.write(hasilPembagian);
	</script>

	<!-- <a href="#" onclick="sayHello()">Klik Disini</a> -->
</body>
</html>
