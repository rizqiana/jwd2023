<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Latihan 5: Layout Page</title>
	<style>
	* {
	  box-sizing: border-box;
	}
	
	body {
	  font-family: Arial, Helvetica, sans-serif;
	}
	
	/* Style the header */
	header {
	  background-color: #666;
	  padding: 30px;
	  text-align: center;
	  font-size: 35px;
	  color: white;
	}
	
	/* Create two columns/boxes that float next to each other */
	nav {
	  float: left;
	  width: 30%;
	  background: #ccc;
	  padding: 20px;
	  height: calc(100vh - 210px);
	}
	
	/* Style the list inside the menu */
	nav ul {
	  list-style-type: none;
	  padding: 0;
	}
	
	article {
	  display: none;
	  float: left;
	  padding: 20px;
	  width: 70%;
	  background-color: #f1f1f1;
	  height: calc(100vh - 210px);
	}
	
	/* Clear floats after the columns */
	section:after {
	  content: "";
	  display: table;
	  clear: both;
	}
	
	/* Style the footer */
	footer {
	  background-color: #777;
	  padding: 10px;
	  text-align: center;
	  color: white;
	}
	
	@media (max-width: 600px) {
	  nav, article {
	    width: 100%;
	    height: auto;
	  }
	}
	</style>
</head>
<body>
	<header>
	  <h2>Kota di Indonesia</h2>
	</header>
	<section>
	  <nav>
	    <ul>
	      <li><a href="#" onclick="showArticle(1)">Jakarta</a></li>
	      <li><a href="#" onclick="showArticle(2)">Depok</a></li>
	      <li><a href="#" onclick="showArticle(3)">Surabaya</a></li>
	    </ul>
	  </nav>
	  <article id="article1">
	    <h1>Jakarta</h1>
	    <p>Ibu kota Jakarta merupakan suatu wilayah yang padat penduduk dan menjadi pusat pemerintahan Republik Indonesia.</p>
	    <p>DKI Jakarta mempunyai banyak tempat rekreasi seperti Pulau Seribu, Taman Impian Jaya Ancol, dan juga beberapa pulau yang ada disekitar teluk Jakarta.</p>
	  </article>
	  <article id="article2">
	    <h1>Depok</h1>
	    <p>Kota Depok merupakan salah satu kota di Indonesia yang terletak di Provinsi Jawa Barat. Kota ini berbatasan langsung dengan Kota Jakarta Selatan dan Kota Bogor. Depok memiliki pertumbuhan pesat dan terkenal dengan kampus-kampus perguruan tinggi.</p>
	    <p>Depok juga memiliki banyak tempat rekreasi, seperti Taman Margonda Raya, Taman Miniatur Indonesia Indah (TMII) Depok, dan Depok Fantasi Water Park.</p>
	  </article>
	  <article id="article3">
	    <h1>Surabaya</h1>
	    <p>Surabaya adalah ibu kota dari Provinsi Jawa Timur, Indonesia. Kota ini merupakan salah satu pusat bisnis, perdagangan, dan industri di Indonesia. Surabaya juga dikenal dengan sebutan "Kota Pahlawan" karena memiliki sejarah perjuangan dalam pergerakan kemerdekaan Indonesia.</p>
	    <p>Kota ini memiliki banyak objek wisata seperti Taman Bungkul, Monumen Kapal Selam, dan Kota Tua Surabaya. Surabaya juga terkenal dengan kuliner khasnya, seperti rujak cingur, rawon, dan soto ayam.</p>
	  </article>
	</section>
	<footer>
	  <p>Footer</p>
	</footer>

	<script>
	  function showArticle(articleNumber) {
	    var articles = document.getElementsByTagName("article");
	    for (var i = 0; i < articles.length; i++) {
	      if (i === articleNumber - 1) {
	        articles[i].style.display = "block";
	      } else {
	        articles[i].style.display = "none";
	      }
	    }
	  }
	</script>
</body>
</html>
