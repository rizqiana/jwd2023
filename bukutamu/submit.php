<?php
// Koneksi ke database
$conn = mysqli_connect("localhost", "root", "", "bukutamu");

// Periksa koneksi
if (!$conn) {
    die("Koneksi gagal: " . mysqli_connect_error());
}

// Periksa apakah form telah dikirim
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Ambil data dari form
    $no = $_POST['no'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $mobile = $_POST['mobile'];

    // Query untuk menyimpan data tamu ke dalam tabel tamu2
    $sql = "INSERT INTO tamu2 (no, name, email, mobile) VALUES ('$no', '$name', '$email', '$mobile')";

    if (mysqli_query($conn, $sql)) {
        // Data tamu berhasil disimpan, arahkan ke halaman data_tamu.php
        header("Location: data_tamu.php");
        exit;
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}

// Tutup koneksi
mysqli_close($conn);
?>
