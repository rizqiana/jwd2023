<?php
// Koneksi ke database
$conn = mysqli_connect("localhost", "root", "", "bukutamu");

// Periksa koneksi
if (!$conn) {
    die("Koneksi gagal: " . mysqli_connect_error());
}

// Periksa apakah form telah disubmit
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Ambil data dari form
    $no = $_POST['no'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $mobile = $_POST['mobile'];

    // Escape nilai yang dikirimkan
    $escapedNo = mysqli_real_escape_string($conn, $no);
    $escapedName = mysqli_real_escape_string($conn, $name);
    $escapedEmail = mysqli_real_escape_string($conn, $email);
    $escapedMobile = mysqli_real_escape_string($conn, $mobile);

    // Query untuk melakukan pembaruan data tamu
    $sql = "UPDATE tamu2 SET name='$escapedName', email='$escapedEmail', mobile='$escapedMobile' WHERE no='$escapedNo'";

    if (mysqli_query($conn, $sql)) {
        // Pembaruan data tamu berhasil, arahkan kembali ke halaman data_tamu.php
        header("Location: data_tamu.php");
        exit;
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}

// Tutup koneksi
mysqli_close($conn);
?>
