<?php
session_start();

// Cek apakah pengguna sudah login
if (isset($_SESSION['username'])) {
    // Jika pengguna sudah login, redirect ke halaman selamat.php
    header('Location: selamat.php');
    exit();
}

// Periksa apakah form telah disubmit
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Ambil data dari form
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Periksa username dan password
    // Ganti kondisi ini dengan logika validasi sesuai kebutuhan aplikasi Anda
    if ($username === 'admin' && $password === 'admin123') {
        // Simpan informasi login ke session
        $_SESSION['username'] = $username;

        // Redirect ke halaman selamat.php
        header('Location: selamat.php');
        exit();
    } else {
        // Jika username atau password salah, tampilkan pesan error
        $error = "Username atau password salah!";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
</head>
<body>
    <h1>Login</h1>

    <?php if (isset($error)) { ?>
        <p><?php echo $error; ?></p>
    <?php } ?>

    <form method="post" action="">
        <label for="username">Username:</label>
        <input type="text" id="username" name="username" required><br><br>
        
        <label for="password">Password:</label>
        <input type="password" id="password" name="password" required><br><br>

        <input type="submit" value="Login">
    </form>
</body>
</html>
