<?php
// Koneksi ke database
$conn = mysqli_connect("localhost", "root", "", "bukutamu");

// Periksa koneksi
if (!$conn) {
    die("Koneksi gagal: " . mysqli_connect_error());
}

// Periksa apakah parameter name tersedia
if (isset($_GET['name'])) {
    $name = $_GET['name'];

    // Query untuk menghapus data tamu berdasarkan name
    $sql = "DELETE FROM tamu2 WHERE name='$name'";

    if (mysqli_query($conn, $sql)) {
        // Data tamu berhasil dihapus, arahkan kembali ke halaman data_tamu.php
        header("Location: data_tamu.php");
        exit;
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}

// Tutup koneksi
mysqli_close($conn);
?>
